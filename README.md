# Try SAP Cloud SDK and OData Mock Service

## How to Run

You must install Node.js before going through steps below

- execute `npm install` in this root directory
- after all dependencies are successfully installed, execute `npm run start:dev` to run the Client or Backend Application that uses Nestjs (TypeScript Framework)
- visit [https://localhost:8080](https://localhost:8080) in the browser, and you will see Hello World!
- cd again to 'mock-server' directory
- execute `npm install`
- then run the Mock Server which is providing dummy resource by executing `npm start` inside the 'mock-server' directory
- visit [https://localhost:3000](https://localhost:3000) and you will see all available API services
- and finally, visit [http://localhost:8080/business-partner](http://localhost:8080/business-partner) and it will show all dummy business partner data

### Tutorial

https://sap.github.io/cloud-sdk/docs/js/tutorials/getting-started/introduction

### Mock Server

https://sap.github.io/cloud-s4-sdk-book/pages/mock-odata.html
