import { Controller, Get, HttpException } from '@nestjs/common';
import { BusinessPartner } from 'services/business-partner-service';
import { BusinessPartnerService } from './business-partner.service';

@Controller('business-partner')
export class BusinessPartnerController {
  constructor(private businessPartnerService: BusinessPartnerService) {}
  @Get()
  async getBusinessPartners() {
    return await this.businessPartnerService
      .getAllBusinessPartners()
      .catch((err) => {
        throw new HttpException(
          `Failed to get business partners - ${err.message}`,
          500,
        );
      });
  }
}
